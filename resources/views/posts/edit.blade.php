@extends('layouts.app')

@section('content')
<div class="border p-4 rounded border-danger border-2">
    <form method="POST" action="/posts/{{$post->id}}">
        @method('PUT')
        @csrf
        <h1 class="text-center fw-semibold">Edit a post</h1>
        <div class="form-group">
            <label class="p-2 fw-semibold" for="title">Title:</label>
            <input type="text" name="title" class="form-control" value={{$post->title}} id="title">
        </div>
        <div class="form-group">
            <label class="p-2 fw-semibold" for="content">Content:</label>
            <textarea class="form-control" name="content" id="content" cols="30" rows="3">{{$post->content}}  
            </textarea>
        </div>
        @if (Auth::user()->id === $post->user_id)
        <div class="mt-2">
            <button type="submit" class="btn btn-primary">Update Post</button>
        </div>
    @endif
    </form>
   </div>
@endsection