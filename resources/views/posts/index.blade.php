@extends('layouts.app')

@section('content')

    @if(count($posts) > 0)
        
            @foreach ($posts as $post)
                <div class="card text-center mb-3">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                        <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                        {{-- Check if there is an authenticated user ti prevent our web app from throwing an error when no user is login --}}
                    </div>

                    @if (Auth::user())
                        @if (Auth::user()->id == $post->user_id)
                            <div>
                                <form method="POST" action="/posts/{{$post->id}}">
                                    @method('PUT')
                                    @csrf
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>
                                    @if ($post->isActive == true) 
                                        <button type="submit" class="btn btn-danger">Archive Post</button>
                                    
                                        
                                    @else
                                    <button type="submit" class="btn btn-success">Unarchive Post</button>
                                    @endif
                                </form>
                            </div>
                        @endif
                        
                    @endif
                </div>
            @endforeach
        
    @else 
        <div>
            <h2>There are no post to show</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
            
    @endif
    
@endsection