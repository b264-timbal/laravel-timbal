@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>

            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif
            @if (Auth::id())
                <button type="submit" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#comment-modal">Comment</button>
                <div class="mt-3">
            @endif
                
            </div>
            <a href="/myPosts" class="card-link text-center">View all posts</a>
        </div>
    </div>

     {{-- Comment Section --}}
     <h1>Comments</h1>
     <div class="card p-3">
        <div class="card-body">
            @foreach ($postComment as $comment)
                <div class="d-flex justify-content-between">
                    <p class="card-text">{{$comment->content}}</p> 
                    <div class="d-flex flex-column align-items-end justify-content-end">
                        <p><b>Posted by: {{$comment->user->name}}</b></p>
                        <p class="text-muted">Posted on: {{$comment->created_at}}</p>
                    </div>
                </div>
                
               
            @endforeach

        </div>

    </div>

    {{-- Modal --}}
    <div class="modal fade modal-dialog modal-dialog-centered" id="comment-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title fs-5" id="staticBackdropLabel">Comment</h1>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
               @if (Auth::id())
               <form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
                @method('PUT')
                @csrf
                <label for="content"></label>
                <textarea class="form-control" name="content" placeholder="Type your comment here..." id="content" cols="30" rows="3"></textarea>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Comment</button>
                  </div>

            </form>
                   
               @endif
            </div>
            
          </div>
        </div>
      </div>
   
    
    
@endsection