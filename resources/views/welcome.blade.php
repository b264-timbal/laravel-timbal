<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        {{-- Bootstrap --}}
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

        <!-- Styles -->
       <style>
            .welcome__main-div {
                width: 100%;
                height: 100%;
            }
            .welcome__logo-div {
                display: flex;
                justify-content: center;
                align-items: flex-start;
                margin: 30px;
            }

            .welcome__logo-div img {
                width: 50%;
            }

            .welcome__featured-div {
                display: flex;
                justify-content: center;
                margin: 2rem;
                padding: 1rem;
                border: 1px solid darkcyan;
                border-radius: 10px;
            }

            .featured_posts {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;

            }

            h1 {
                display: flex;
                justify-content: center;
                color: rgb(32, 32, 32);
            }

            a {
                text-decoration: none;
                color: Red;
            }

       </style>
    </head>
    <body class="antialiased">
        <div class="welcome__main-div">
            <div class="welcome__logo-div">
                <img src="/images/laravel-logo-full.png" alt="laravel-logo">
            </div>
            <h1>Featured Posts</h1>
            @foreach ($posts as $post)
                <div class="welcome__featured-div">
                    <div class="featured_posts">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                        <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    </div>
                </div>
                
            @endforeach
        </div>
    </body>
</html>
