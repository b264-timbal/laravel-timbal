<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Auth (middleware) -access the authenticated user via Auth Facades.
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for blog post creation.
    public function create()
        {
            return view('posts.create');
        }

    public function store(Request $request)
        {
            if(Auth::user()) {
                // instantiate a new Post object from the Post Model
                $post = new Post;

                // Define the properties of the $post object using the recieved from data.
                $post->title = $request->input('title');
                $post->content = $request->input('content');

                // get tge id of the authenticated user and set it as the foreign key user_id of the new post.
                $post->user_id = (Auth::user()->id);
                // save this post object into the database.
                $post->save();

                return redirect('/posts');
            } else {
                return redirect('/login');
            }
        }

        // action that will return a view showing all blog posts.
        public function index() 
            {
                // $posts = Post::where('isActive', true)->get();
                $posts = Post::all();
                return view('posts.index')->with('posts', $posts);
                
            }

        

        // Mini Activity.
    //1. In the create_posts_table.php add an isActive field with a Boolean data type and a default value "true"

    //2. run command: to refresh the migrations and to add the isActive field into the posts table.

    //3. Refactor the index() in PostController so that it will only return the active posts from the posts table regardless of the user.


    // action for showing only the posts authored by the user.

        public function myPosts()
            {
                if(Auth::user()) {
                $posts = Auth::user()->posts;
                return view('posts.index')->with('posts', $posts);
                } else {
                    return redirect('/login');
                }
            } 

        public function featured()
            {
                $posts = Post::where('isActive', true)->inRandomOrder()->limit(3)->get();
                return view('welcome')->with('posts', $posts);

            }
        // action that will return a view showing a specific post using the URL parameter $id.
        public function show($id) 
            {
                $post = Post::find($id);
                $postComment = PostComment::where('post_id', $id)->get();
                return view('posts.show')
                        ->with('post', $post)
                        ->with('postComment', $postComment);
            }

        // edit controller
        public function edit($id)
            {
                $post = Post::find($id);
                return view('posts.edit')->with('post', $post);
            }
        
            // action to update a specific post by an authenticated user.
        public function update(Request $request, $id)
            {
                $post = Post::find($id);

                $post->title = $request->input('title');
                $post->content = $request->input('content');
                $post->save();
                
                return redirect('/posts/'.$id);
            }
            
            // define action/function for deleting post's
            public function destroy($id)
                {
                    $post = Post::find($id);

                    if(Auth::user()->id === $post->user_id) {
                        $post->delete();
                    }

                    return redirect('/posts');
                }

            public function archive($id)
                {
                    $post = Post::find($id);
                    if ($post->isActive == true) {
                        $post->isActive = false;
                        $post->save();
                    } else {
                        $post->isActive = true;
                        $post->save();
                    }

                    return redirect('/myPosts');
                }
            public function like($id)
                {
                    $post = Post::find($id);
                    $user_id = Auth::user()->id;
                    // if an authenticated user is not the post author
                    if($post->user_id != $user_id) {
                        // this checks if a post like has been made by a certain user before.
                        if($post->likes->contains("user_id", $user_id)){
                            // Delete the like made by this user to unlike this post.
                            PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                        } else {
                            // create a new like record to like a specific post.
                            // instantiate a new PostLike object from the PostLike model.
                            $postLike = new PostLike;

                            // define the properties of the $postLike object
                            $postLike->post_id = $post->id;
                            $postLike->user_id = $user_id;

                            // save this $postLike object into the database
                            $postLike->save();
                        }
                        // redirect the user back to the same post.
                        return redirect("/posts/$id");
                    }
                }

            public function comment(Request $request, $id)
                {
                    $post = Post::find($id);

                        
                        $postComment = new PostComment;

                        $postComment->content = $request->input('content');
                        $postComment->user_id = (Auth::user()->id);
                        $postComment->post_id = $post->id;
                       
                        $postComment->save();
        
                        return redirect("/posts/$id");
                    
                }

           

}
