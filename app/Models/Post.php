<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    // This is to defines that the Post belongs to a specific User.
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\PostLike');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\PostComment');
    }
}
